var app = angular.module('todoapp', ['ionic', 'firebase']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider){

  $stateProvider.state('lista', {
    url: '/lista',
    templateUrl: 'templates/lista.html',
    controller: 'ListaCtrl'
  });

  $stateProvider.state('cadastro', {
    url: '/cadastro',
    templateUrl: 'templates/cadastro.html',
    controller: 'CadastroCtrl'
  });

  $stateProvider.state('edita', {
    url: '/edita/:id',
    templateUrl: 'templates/cadastro.html',
    controller: 'EditaCtrl'
  });

  $stateProvider.state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  });

  $stateProvider.state('registro', {
    url: '/registro',
    templateUrl: 'templates/registro.html',
    controller: 'RegistroCtrl'
  });

  $urlRouterProvider.otherwise('/login');

})

app.controller('IndexCtrl', function($scope) {
  // $scope.name = "Hello World";

  //JSON(STRING,NUMBER,BOOLEAN,NULL,OBJECT)
  // var tarefa1 = {
  //   nome: "Tarefa 1",
  //   descricao: "Fazer alguma coisa.",
  //   concluida: false
  // }
  // $scope.search = "";
});

app.controller('LoginCtrl', function($scope, $state, $firebaseAuth) {

  $firebaseAuth().$onAuthStateChanged(function(firebaseUser){
      if(firebaseUser){
        $state.go('lista');
      }
  });

  $scope.entrar = function(user){
    $firebaseAuth()
      .$signInWithEmailAndPassword(user.email, user.password)
      .then(function(user){
         $state.go('lista');
      })
      .catch(function(error){
        alert(error.message);
      })
  }
});

app.controller('RegistroCtrl', function($scope, $state, $firebaseAuth) {
  $scope.registrar = function(user){
    $firebaseAuth()
      .$createUserWithEmailAndPassword(user.email, user.password)
      .then(function(user){
         $state.go('login');
      })
      .catch(function(error){
        alert(error.message);
      })
  }
});

app.controller('ListaCtrl', function($scope, TarefaService, $firebaseArray, $firebaseAuth, $state) {

  //$scope.tarefas =  TarefaService.read();

  // var t1 = {
  //   nome: "Tarefa 1",
  //   descricao: "Desc da Tarefa 1",
  //   concluida: false
  // }

  // TarefaService.create(t1);
  $firebaseAuth().$onAuthStateChanged(function(firebaseUser){
    var ref = firebase.database().ref().child('tarefas');
    var query = ref.orderByChild('uid').equalTo(firebaseUser.uid);
    $scope.tarefas = $firebaseArray(query);
  });
  $scope.apagar = function(id){
    //1.Recuperar o objeto pelo id
    var obj = $scope.tarefas.$getRecord(id);

    //2.Exclusão pelo objeto
    $scope.tarefas.$remove(obj);
  }


  $scope.editar = function(id){

    $state.go('edita', {id : id});

  }
});

app.controller('CadastroCtrl', function($scope, $firebaseArray,$firebaseAuth, $state) {

  $scope.todo = {
    concluida: false
  };

  $scope.salvar = function(todo){

    $firebaseAuth().$onAuthStateChanged(function(firebaseUser){

        todo.uid = firebaseUser.uid;

        var ref = firebase.database().ref().child('tarefas');
        $firebaseArray(ref).$add(todo);
  
        $state.go('lista');
        
    });

  }
 
});

app.controller('EditaCtrl', function($scope, $firebaseObject, $state, $stateParams) {

    var id =  $stateParams.id;

    var ref = firebase.database().ref().child('tarefas').child(id);

    $scope.todo = $firebaseObject(ref);
    $scope.salvar = function(todo){
     
      $scope.todo = todo;
      $scope.todo.$save();
  
      $state.go('lista');
    }
   
  });

app.factory('TarefaService', function() {

  var tarefas = []; // database

  return {
    read: function() {
      return tarefas;
    },

    create: function(tarefa) {
      tarefas.push(tarefa);
    }
  }
  
})